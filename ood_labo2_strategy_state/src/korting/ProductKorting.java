package korting;

import java.math.BigDecimal;

public interface ProductKorting {
	
	BigDecimal berekenKorting(int aantalDagen, BigDecimal totaalHuurprijs);
	String getType();

}
