package korting;

import java.math.BigDecimal;

import enums.TKorting;

public class RegularCustomerKorting implements ProductKorting {

	@Override
	public BigDecimal berekenKorting(int aantalDagen, BigDecimal totaalHuurprijs) {
		BigDecimal korting = new BigDecimal(aantalDagen);
		return korting;
	}

	@Override
	public String getType() {
		return TKorting.REGULARCUSTOMER.getKortingsType();
	}

}
