package korting;

import java.math.BigDecimal;

import enums.TKorting;

public class KwantumKorting implements ProductKorting {

	@Override
	public BigDecimal berekenKorting(int aantalDagen, BigDecimal totaalHuurprijs) {
		BigDecimal multiplier = new BigDecimal(0.25);
		return totaalHuurprijs.multiply(multiplier);
	}

	@Override
	public String getType() {
		return TKorting.KWANTUM.getKortingsType();
	}

}
