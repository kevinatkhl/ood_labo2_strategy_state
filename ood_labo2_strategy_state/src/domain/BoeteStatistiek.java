package domain;

import java.math.BigDecimal;

public class BoeteStatistiek implements BoeteObserver {
	
	private BigDecimal boetebedrag;
	private int totaalBoetes;
	
	public BoeteStatistiek() {
		boetebedrag = new BigDecimal(0.0);
		setTotaalBoetes(0);
	}

	public BigDecimal getBoetebedrag() {
		return boetebedrag;
	}

	public void setBoetebedrag(BigDecimal boetebedrag) {
		this.boetebedrag = boetebedrag;
	}

	public int getTotaalBoetes() {
		return totaalBoetes;
	}

	public void setTotaalBoetes(int totaalBoetes) {
		this.totaalBoetes = totaalBoetes;
	}

	public String toString() {
		return "BOETES: " + getTotaalBoetes() + "\nTOTAAL BEDRAG: " + getBoetebedrag();
	}

	@Override
	public void update(BigDecimal bd) {
		setTotaalBoetes(getTotaalBoetes() + 1);
		setBoetebedrag(getBoetebedrag().add(bd));
	}

}
