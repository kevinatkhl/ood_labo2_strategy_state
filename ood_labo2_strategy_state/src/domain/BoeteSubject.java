package domain;

import java.math.BigDecimal;

public interface BoeteSubject {
	
	void register(BoeteObserver b);
	void unregister(BoeteObserver b);
	void updateAllObservers(BigDecimal bd);

}
