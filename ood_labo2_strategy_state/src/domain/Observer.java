package domain;

import producten.Product;

public interface Observer {
	
	void update(Product product);

}
