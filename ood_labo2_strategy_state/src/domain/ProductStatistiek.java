package domain;

import java.util.HashMap;
import java.util.Map;

import enums.TProduct;
import producten.Film;
import producten.Muziek;
import producten.Product;
import producten.Spel;

public class ProductStatistiek implements Observer {
	
	private int filmAantal,
				muziekAantal,
				spelAantal;
	private Map<String, Product> producten;
	
	public ProductStatistiek() {
		setFilmAantal(0);
		setMuziekAantal(0);
		setSpelAantal(0);
		producten = new HashMap<String, Product>();
	}

	public int getFilmAantal() {
		return filmAantal;
	}

	public void setFilmAantal(int filmAantal) {
		this.filmAantal = filmAantal;
	}
	
	public void addFilmAantal() {
		this.filmAantal = getFilmAantal() + 1;
	}

	public int getMuziekAantal() {
		return muziekAantal;
	}

	public void setMuziekAantal(int muziekAantal) {
		this.muziekAantal = muziekAantal;
	}
	
	public void addMuziekAantal() {
		this.muziekAantal = getMuziekAantal() + 1;
	}

	public int getSpelAantal() {
		return spelAantal;
	}

	public void setSpelAantal(int spelAantal) {
		this.spelAantal = spelAantal;
	}
	
	public void addSpelAantal() {
		this.spelAantal = getSpelAantal() + 1;
	}
	
	public String toString()
	{
		StringBuilder filmSB = new StringBuilder();
		StringBuilder muziekSB = new StringBuilder();
		StringBuilder spelSB = new StringBuilder();
		for (String s : producten.keySet())
		{
			if (producten.get(s) instanceof Film) {
				addFilmAantal();
				filmSB.append("*");
			}
			else if (producten.get(s) instanceof Muziek) {
				addMuziekAantal();
				muziekSB.append("*");
			}
			else if (producten.get(s) instanceof Spel) {
				addSpelAantal();
				spelSB.append("*");
			}
		}
		return TProduct.FILM.name() + ": " + filmSB.toString()
				+ "\n" + TProduct.MUZIEK.name() + ": " + muziekSB.toString()
				+ "\n" + TProduct.SPEL.name() + ": " + spelSB.toString();
	}

	@Override
	public void update(Product product) {
		producten.put(product.getId(), product);
	}

}
