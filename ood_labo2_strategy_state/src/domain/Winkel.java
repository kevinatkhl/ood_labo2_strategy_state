package domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import klanten.Klant;
import korting.ProductKorting;
import producten.Product;

public class Winkel implements Subject, BoeteSubject {
	
	private Map<String,Product> producten;
	private Map<Integer,Klant> klanten;
	private List<Observer> observers;
	private List<BoeteObserver> boeteObservers;
	private Product recentProduct;
	
	public Winkel()
	{
		producten = new HashMap<String,Product>();
		klanten = new HashMap<Integer,Klant>();
		observers = new ArrayList<Observer>();
		boeteObservers = new ArrayList<BoeteObserver>();
	}
	
	public void addProduct(Product product)
			throws DomainException{
		if(product == null)
			throw new DomainException("product should not be null");
		if (getProduct(product.getId()) == null) {
			producten.put(product.getId(), product);
			setRecentProduct(product);
			updateObservers();
		} else {
			throw new DomainException("product already exists!");
		}
	}
	
	public Product getProduct(String id)
	{
		Set<String> productenSet = producten.keySet();
		Product product = null;
		for(String p: productenSet)
		{
			if (producten.get(p).getId().equals(id))
			{
				product = producten.get(p);
			}
		}
		return product;		
	}
	
	public Product getRecentProduct() {
		return recentProduct;
	}
	
	public void setRecentProduct(Product recentProduct) {
		this.recentProduct = recentProduct;
	}
	
	public Klant getKlant(int id) {
		Set<Integer> klantenSet = klanten.keySet();
		Klant klant = null;
		for (Integer k : klantenSet) {
			if (klanten.get(k).getUserId() == id) {
				klant = klanten.get(k);
			}
		}
		return klant;
	}
	
	public String getKlantSubscription(int id) {
		Klant klant = getKlant(id);
		if (klant != null)
			return klant.printFQ();
		else
			return "Klant is niet geregistreerd.";
	}
	
	public String getProductTitle(String id)
	{
		String title = "";
		Product product = getProduct(id);
		if(product != null)
		{
			title = product.getName();
		}
		else
		{
			title = "no product with this id";
		}
		return title;
	}
	
	public BigDecimal getProductRentalPrice(String id, int nrDays)
	{
		BigDecimal price = new BigDecimal(0.0);
		Product product = getProduct(id);
		if(product != null)
		{
			price = product.calculateRentalPrice(nrDays);
		}
		return price;
	}
	
	public void setProductDiscount(String id, ProductKorting eenKorting) {
		Product product = getProduct(id);
		if (product != null)
			product.setHuidigeKorting(eenKorting);
	}
	
	public BigDecimal getProductDiscount(String id, int aantalDagen, BigDecimal totaalHuurprijs) {
		Product product = getProduct(id);
		BigDecimal price = new BigDecimal(0);
		if (product != null) {
			price = product.berekenKorting(aantalDagen, totaalHuurprijs);
		}
		return price;
	}
	
	public String toString()
	{
		String winkelitems = "";
		Set<String> productenSet = producten.keySet();
		for(String product: productenSet)
		{
			winkelitems += product + ": "+producten.get(product)+"\n";
		}
		return winkelitems;
	}
	
	public ArrayList<Product> getProducten()
	{
		ArrayList<Product> productenlijst = new ArrayList<Product>();
		Set<String> productenSet = producten.keySet();
		for(String product: productenSet)
		{
			productenlijst.add(producten.get(product));
		}
		return productenlijst;
	}
	
	public String loanItem(String id) {
		Product product = getProduct(id);
		if (product != null) {
			return product.uitlenen();
		} else {
			return "Product bestaat niet.";
		}
	}
	
	public String repairItem(String id) {
		Product product = getProduct(id);
		if (product != null) {
			return product.herstellen();
		} else {
			return "Product bestaat niet.";
		}
	}
	
	public String returnItem(String id, boolean beschadigd) {
		Product product = getProduct(id);
		if (product != null) {
			product.setBeschadigd(beschadigd);
			return product.terugbrengen();
		} else {
			return "Product bestaat niet.";
		}
	}
	
	public String removeItem(String id) {
		Product product = getProduct(id);
		if (product != null) {
			return product.verwijderen();
		} else {
			return "Product bestaat niet.";
		}
	}
	
	public Map<Integer, Klant> getKlanten() {
		return klanten;
	}
	
	public ArrayList<Klant> getKlantenList() {
		ArrayList<Klant> klantenlijst = new ArrayList<Klant>();
		Set<Integer> klantenSet = getKlanten().keySet();
		for (Integer klant : klantenSet) {
			klantenlijst.add(klanten.get(klant));
		}
		return klantenlijst;
	}
	
	public boolean klantExists(int id) {
		Klant k = getKlant(id);
		return k != null;
	}
	
	public void addKlant(Klant k) throws IllegalArgumentException {
		if (!klantExists(k.getUserId()))
			getKlanten().put(k.getUserId(), k);
		else
			throw new IllegalArgumentException("Klant bestaat al.");
	}
	
	public List<Observer> getObservers() {
		return observers;
	}
	
	@Override
	public void updateObservers() {
		for (Observer o : getObservers()) {
			o.update(getRecentProduct());
		}
	}

	@Override
	public void register(Observer o) throws IllegalArgumentException {
		if (o == null)
			throw new IllegalArgumentException("Observer cannot be null.");
		if (!getObservers().contains(o))
			getObservers().add(o);
	}

	@Override
	public void unregister(Observer o) throws IllegalArgumentException {
		if (o == null) throw new IllegalArgumentException("Observers cannot be null.");
		if (getObservers().contains(o)) {
			int observerIndex = getObservers().indexOf(o);
			getObservers().remove(observerIndex);
		}
	}
	
	@Override
	public void register(BoeteObserver b) {
		if (b == null) throw new IllegalArgumentException("Observers cannot be null.");
		if (!boeteObservers.contains(b))
			boeteObservers.add(b);
	}

	@Override
	public void unregister(BoeteObserver b) {
		if (b == null) throw new IllegalArgumentException("Observers cannot be null.");
		if (boeteObservers.contains(b)) {
			int bObserverIndex = boeteObservers.indexOf(b);
			boeteObservers.remove(bObserverIndex);
		}
	}

	@Override
	public void updateAllObservers(BigDecimal bd) {
		for (BoeteObserver b : boeteObservers) {
			b.update(bd);
		}
	}

}