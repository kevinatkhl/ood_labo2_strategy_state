package domain;

import java.math.BigDecimal;

public interface BoeteObserver {

	void update(BigDecimal bd);
	
}
