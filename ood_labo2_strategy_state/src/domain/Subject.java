package domain;

public interface Subject {
	
	void updateObservers();
	void register(Observer o);
	void unregister(Observer o);

}
