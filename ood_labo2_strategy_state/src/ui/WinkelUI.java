package ui;

import java.math.BigDecimal;

import db.DBHandler;
import db.DBReader;
import db.DBWriter;
import db.DbException;
import domain.BoeteStatistiek;
import domain.DomainException;
import domain.ProductStatistiek;
import domain.Winkel;
import enums.TDBHandler;
import enums.TKorting;
import enums.TProduct;
import factories.DBFactory;
import factories.KlantFactory;
import factories.KortingFactory;
import factories.ProductFactory;

import javax.swing.JOptionPane;

import klanten.Klant;
import korting.ProductKorting;
import producten.Product;

public class WinkelUI {
	
	private Winkel winkel;
	private ProductStatistiek prodStats;
	private BoeteStatistiek boeteStats;
	
	public void start()
	{
		winkel		= new Winkel();
		prodStats	= new ProductStatistiek();
		boeteStats	= new BoeteStatistiek();
		
		Object dbKeuzeSelector	= JOptionPane.showInputDialog(null, 
				"Database Keuze", "Mogelijke keuzes", 
				JOptionPane.QUESTION_MESSAGE, null, TDBHandler.values(), null);
		
		TDBHandler dbKeuze = (TDBHandler) dbKeuzeSelector;
		
		String fileString	= JOptionPane.showInputDialog("Hoe noemt uw database bestand?");
		
		DBHandler dbHandler = null;
		
		try {
			dbHandler = new DBHandler(fileString, winkel);
		} catch (DbException e) {
			e.printStackTrace();
		}
		
		DBFactory dbFactory = DBFactory.getInstance();
		DBReader dbReader;
		DBWriter dbWriter;
		
		try {
			dbReader = dbFactory.createReader(dbKeuze.getFullyQualifiedNameReader());
			dbWriter = dbFactory.createWriter(dbKeuze.getFullyQualifiedNameWriter());
			dbHandler.setReader(dbReader);
			dbHandler.setWriter(dbWriter);
		} catch (DomainException e) {
			e.printStackTrace();
		}
		
		try {
			dbHandler.getReader().read();
		} catch (DbException e) {
			JOptionPane.showMessageDialog(null, "Error reading from file.");
		}
		
		winkel.register(prodStats);
		winkel.register(boeteStats);
		
		int menuKeuze = -1;
		int subMenuKeuze = -1;
		
		do
		{
			String mainOptionSelector = JOptionPane.showInputDialog(boeteStats.toString() + "\n\n" + displayMainMenu());
			menuKeuze = Integer.parseInt(mainOptionSelector);
			if (mainOptionSelector == null)
				menuKeuze = 0;
			else
			{
				String subOptionSelector = null;
				switch (menuKeuze)
				{
				case 1:
					subOptionSelector = JOptionPane.showInputDialog(displayProductsMenu());
					subMenuKeuze = Integer.parseInt(subOptionSelector);
					if (subOptionSelector == null)
					{
						subMenuKeuze = 0;
						break;
					}
					else
					{
						switch (subMenuKeuze)
						{
						case 1: addItem(winkel); break;
						case 2: JOptionPane.showMessageDialog(null, "not implemented"); break;
						case 3: JOptionPane.showMessageDialog(null, "not implemented"); break;
						case 4: showItem(winkel); break;
						case 5: showItems(winkel); break;
						default: break;
						}
					}
					break;
				case 2:
					subOptionSelector = JOptionPane.showInputDialog(displayCustomersMenu());
					subMenuKeuze = Integer.parseInt(subOptionSelector);
					if (subOptionSelector == null)
					{
						subMenuKeuze = 0;
						break;
					}
					else
					{
						switch (subMenuKeuze)
						{
						case 1: addKlant(winkel); break;
						case 2: JOptionPane.showMessageDialog(null, "not implemented"); break;
						case 3: JOptionPane.showMessageDialog(null, "not implemented"); break;
						case 4: JOptionPane.showMessageDialog(null, "not implemented"); break;
						case 5: JOptionPane.showMessageDialog(null, "not implemented"); break;
						default: break;
						}
					}
					break;
				case 3:
					subOptionSelector = JOptionPane.showInputDialog(displayProductStateMenu());
					subMenuKeuze = Integer.parseInt(subOptionSelector);
					if (subOptionSelector == null)
					{
						subMenuKeuze = 0;
						break;
					}
					else
					{
						switch (subMenuKeuze)
						{
						case 1: loanItem(winkel); break;
						case 2: returnItem(winkel); break;
						case 3: repairItem(winkel); break;
						case 4: removeItem(winkel); break;
						case 5: showPrice(winkel); break;
						}
					}
					break;
				case 4:
					subOptionSelector = JOptionPane.showInputDialog(displaySubscriptionMenu());
					subMenuKeuze = Integer.parseInt(subOptionSelector);
					if (subOptionSelector == null)
					{
						subMenuKeuze = 0;
						break;
					}
					else
					{
						switch (subMenuKeuze)
						{
						case 1: registerKlant(winkel); break;
						case 2: JOptionPane.showMessageDialog(null, "not implemented"); break;
						case 3: getKlantUpdates(winkel); break;
						default: break;
						}
					}
					break;
				case 5: getWinkelStats(); break;
				default: break;
				}
			}
		} while (menuKeuze != 0);
		
		try {
			dbHandler.getWriter().write();
		} catch (DbException e) {
			JOptionPane.showMessageDialog(null, "Error writing to file.");
		}
		
	}
	
	private void getWinkelStats() {
		JOptionPane.showMessageDialog(null, prodStats.toString());
	}
	
	private void registerKlant(Winkel winkel) {
		int id = Integer.parseInt(JOptionPane.showInputDialog("Enter the klant id: "));
		Klant k = winkel.getKlant(id);
		winkel.register(k);
	}
	
	private void getKlantUpdates(Winkel winkel) {
		int id = Integer.parseInt(JOptionPane.showInputDialog("Enter the klant id: "));
		JOptionPane.showMessageDialog(null, winkel.getKlantSubscription(id));
	}
	
	private void loanItem(Winkel winkel) {
		String id = JOptionPane.showInputDialog("Enter the id: ");
		int klantId = Integer.parseInt(JOptionPane.showInputDialog("Enter klant id: "));
		if (!winkel.klantExists(klantId)) {
			String klantNaam = JOptionPane.showInputDialog("Enter klantnaam: ");
			String klantEmail = JOptionPane.showInputDialog("Enter klant email: ");
			Klant klant = new Klant(klantNaam, klantEmail);
			winkel.addKlant(klant);
		}
		JOptionPane.showMessageDialog(null, winkel.loanItem(id));
	}
	
	private void returnItem(Winkel winkel) {
		String id = JOptionPane.showInputDialog("Enter the id: ");
		boolean beschadigd = Boolean.parseBoolean(JOptionPane.showInputDialog("Item damaged? (true/false)"));
		boolean telaat = Boolean.parseBoolean(JOptionPane.showInputDialog("Te laat teruggebracht? (true/false)"));
		if (telaat)
		{
			int aantalDagen = Integer.parseInt(JOptionPane.showInputDialog("Hoeveel dagen te laat?"));
			double boeteBedrag = aantalDagen >= 5 ? 3 + (aantalDagen - 5) * 3 : 0.0;
			BigDecimal boete = new BigDecimal(boeteBedrag);
			winkel.updateAllObservers(boete);
			JOptionPane.showMessageDialog(null, "Boete van: " + boete.toString());
		}
		JOptionPane.showMessageDialog(null, winkel.returnItem(id, beschadigd));
	}
	
	private void repairItem(Winkel winkel) {
		String id = JOptionPane.showInputDialog("Enter the id: ");
		JOptionPane.showMessageDialog(null, winkel.repairItem(id));
	}
	
	private void removeItem(Winkel winkel) {
		String id = JOptionPane.showInputDialog("Enter the id: ");
		JOptionPane.showMessageDialog(null, winkel.removeItem(id));
	}
	
	private void addItem(Winkel winkel)
	{
		String title = JOptionPane.showInputDialog("Enter the title:");
		String id = JOptionPane.showInputDialog("Enter the id:");
		String waardeString = JOptionPane.showInputDialog("Enter the value: ");
		BigDecimal waarde = new BigDecimal(Double.parseDouble(waardeString));
		Object choice = JOptionPane.showInputDialog(null,
				"What type:", "Possible types",
				JOptionPane.QUESTION_MESSAGE, null, TProduct.values(), null);
		TProduct type = (TProduct) choice;
		try
		{
			ProductFactory productFactory = ProductFactory.getInstance();
			Product product = productFactory.create(type.getFullyQualifiedName(), id, title, waarde);
			winkel.addProduct(product);
		} catch (DomainException e) { JOptionPane.showMessageDialog(null, "failed the create product."); }
	}
	
	private void addKlant(Winkel winkel)
	{
		String naam = JOptionPane.showInputDialog("Enter the name: ");
		String email = JOptionPane.showInputDialog("Enter the email: ");
		try
		{
			KlantFactory klantFactory = KlantFactory.getInstance();
			Klant klant = klantFactory.createKlant(naam, email);
			winkel.addKlant(klant);	
		}
		catch (DomainException e)
		{
			JOptionPane.showMessageDialog(null, "Failed to create klant correctly.");
		}
	}
	
	private void showItem(Winkel winkel)
	{
		String id = JOptionPane.showInputDialog("Enter the id:");
		JOptionPane.showMessageDialog(null, winkel.getProductTitle(id));
	}
	
	private void showItems(Winkel winkel)
	{
		JOptionPane.showMessageDialog(null, winkel);
	}
	
	private void showPrice(Winkel winkel)
	{
		String id = JOptionPane.showInputDialog("Enter the id: ");
		String daysString = JOptionPane.showInputDialog("Enter the number of days: ");
		int days = Integer.parseInt(daysString);
		
		Object kortingsKeuze = JOptionPane.showInputDialog(null,
				"Keuze Korting: ", "Mogelijke kortingen: ", 
				JOptionPane.QUESTION_MESSAGE, null, TKorting.values(), null);
		
		TKorting korting = (TKorting) kortingsKeuze;
		
		try
		{
			KortingFactory kortingFactory = KortingFactory.getInstance();
			ProductKorting kortingType = kortingFactory.create(korting.getFullyQualifiedName());
			winkel.setProductDiscount(id, kortingType);			
		}
		catch (DomainException e)
		{
			e.printStackTrace();
		}
		
		BigDecimal totaalHuurprijs = winkel.getProductRentalPrice(id, days);
		BigDecimal totaalKorting = winkel.getProductDiscount(id, days, totaalHuurprijs);
		JOptionPane.showMessageDialog(null, "Huurprijs: " + totaalHuurprijs.subtract(totaalKorting).toString() + " euro.");
	}
	
	public String displayMainMenu()
	{
		String[] menu = 
			{ 
				"[1] PRODUCTS",
				"[2] CUSTOMERS",
				"[3] PRODUCT ACTIONS",
				"[4] SUBSCRIPTIONS",
				"[5] STATISTICS",
				"[0] QUIT"
			};
		return createMenu(menu);
	}
	
	public String displayProductsMenu()
	{
		String[] menu =
			{
				"[1] ADD PRODUCT",
				"[2] DELETE PRODUCT",
				"[3] MODIFY PRODUCT",
				"[4] DISPLAY PRODUCT",
				"[5] LIST PRODUCTS"
			};
		return createMenu(menu);
	}
	
	public String displayCustomersMenu()
	{
		String[] menu =
			{
				"[1] ADD CUSTOMER",
				"[2] DELETE CUSTOMER",
				"[3] MODIFY CUSTOMER",
				"[4] DISPLAY CUSTOMER",
				"[5] LIST CUSTOMERS"
			};
		return createMenu(menu);
	}
	
	public String displayProductStateMenu()
	{
		String[] menu =
			{
				"[1] LOAN PRODUCT",
				"[2] RETURN PRODUCT",
				"[3] REPAIR PRODUCT",
				"[4] REMOVE PRODUCT",
				"[5] RENTAL PRICE"
			};
		return createMenu(menu);
	}
	
	public String displaySubscriptionMenu()
	{
		String[] menu =
			{
				"[1] SUBSCRIBE CUSTOMER",
				"[2] UNSUBSCRIBE CUSTOMER",
				"[3] CUSTOMER UPDATES"
			};
		return createMenu(menu);
	}
	
	public String createMenu(String[] menuOptions)
	{
		StringBuilder menu = new StringBuilder();
		for (String option : menuOptions)
		{
			menu.append(option + "\n");
		}
		return menu.toString();
	}
	
}
