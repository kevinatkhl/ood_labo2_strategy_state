package enums;

public enum TKorting {
	
	GEEN ("GK", "korting.GeenKorting"),
	KWANTUM ("KK", "korting.KwantumKorting"),
	REGULARCUSTOMER ("RC", "korting.RegularCustomerKorting");
	
	private String TKorting;
	private String fullyQualifiedName;
	
	private TKorting(String TKorting, String fullyQualifiedName)
	{
		this.TKorting = TKorting;
		this.fullyQualifiedName = fullyQualifiedName;
	}
	
	public String getKortingsType()	{ return TKorting; }
	public String getFullyQualifiedName() { return fullyQualifiedName; }

}
