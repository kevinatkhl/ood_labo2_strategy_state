package enums;

public enum TProductState {

	UITLEENBAAR ("U", "states.UitleenbaarState"),
	UITGELEEND ("G", "states.UitgeleendState"),
	BESCHADIGD ("B", "states.BeschadigdState"),
	VERWIJDERD ("V", "states.VerwijderdState");
	
	private String TProductState;
	private String fullyQualifiedName;
	
	private TProductState(String TProductState, String fullyQualifiedName)
	{
		this.TProductState = TProductState;
		this.fullyQualifiedName = fullyQualifiedName;
	}
	
	public String getProductState()
	{
		return TProductState;
	}

	public String getFullyQualifiedName() {
		return fullyQualifiedName;
	}
	
}
