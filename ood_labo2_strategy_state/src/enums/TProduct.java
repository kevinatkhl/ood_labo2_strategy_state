package enums;

public enum TProduct {
	
	FILM ("F", "producten.Film"),
	MUZIEK ("M", "producten.Muziek"),
	SPEL ("S", "producten.Spel");
	
	private String TProduct;
	private String fullyQualifiedName;
	
	private TProduct(String TProduct, String fullyQualifiedName) {
		this.TProduct = TProduct;
		this.fullyQualifiedName = fullyQualifiedName;
	}
	
	public String getProductType() {
		return TProduct;
	}
	
	public String getFullyQualifiedName() {
		return fullyQualifiedName;
	}

}
