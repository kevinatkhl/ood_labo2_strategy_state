package enums;

public enum TDBHandler {
	
	XML ("XML", "db.xml.XMLReader", "db.xml.XMLWriter"),
	TEXT ("TXT", "db.txt.TXTReader", "db.txt.TXTWriter"),
	EXCEL ("XLS", "db.xls.XLSReader", "db.xls.XLSWriter");
	
	private String TDBHandler;
	private String fullyQualifiedNameReader;
	private String fullyqualifiedNameWriter;
	
	private TDBHandler(String TDBHandler, String fullyQualifiedNameReader, String fullyQualifiedNameWriter)
	{
		this.TDBHandler = TDBHandler;
		this.fullyQualifiedNameReader = fullyQualifiedNameReader;
		this.fullyqualifiedNameWriter = fullyQualifiedNameWriter;
	}
	
	public String getTypeDBHandler() { return TDBHandler; }
	public String getFullyQualifiedNameReader() { return fullyQualifiedNameReader; }
	public String getFullyQualifiedNameWriter() { return fullyqualifiedNameWriter; }

}
