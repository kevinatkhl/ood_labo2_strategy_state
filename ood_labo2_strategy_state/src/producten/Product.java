package producten;

import java.math.BigDecimal;

import korting.GeenKorting;
import korting.ProductKorting;
import states.BeschadigdState;
import states.ProductState;
import states.UitgeleendState;
import states.UitleenbaarState;
import states.VerwijderdState;
import domain.DomainException;

public abstract class Product {
	
	private String id;
	private String name;
	
	private boolean beschadigd;
	
	private BigDecimal waarde;
	
	private ProductState	uitleenbaarState,
							uitgeleendState,
							beschadigdState,
							verwijderdState;
	
	private ProductState	huidigeStaat;
	
	private ProductKorting	huidigeKorting;
	
	public Product(String id, String name, BigDecimal waarde) throws DomainException
	{
		setId(id);
		setName(name);
		setBeschadigd(false);
		setWaarde(waarde);
		
		uitleenbaarState = new UitleenbaarState(this);
		uitgeleendState = new UitgeleendState(this);
		beschadigdState = new BeschadigdState(this);
		verwijderdState = new VerwijderdState(this);

		setHuidigeStaat(uitleenbaarState);
		setHuidigeKorting(new GeenKorting());
	}
	
	public String getId() {
		return id;
	}

	private void setId(String id) 
		throws DomainException
	{
		if(id == null)
			throw new DomainException("provide valid id");
		this.id = id;
	}

	public String getName() {
		return name;
	}

	private void setName(String name) 
		throws DomainException
	{
		if(name == null)
			throw new DomainException("provide valid name");
		this.name = name;
	}
	
	public BigDecimal getWaarde() {
		return waarde;
	}

	public void setWaarde(BigDecimal waarde) {
		this.waarde = waarde;
	}

	public ProductState getHuidigeStaat() {
		return huidigeStaat;
	}
	
	public String getHuidigeStaatType() {
		return getHuidigeStaat().getStateType();
	}

	public void setHuidigeStaat(ProductState huidigeStaat) {
		this.huidigeStaat = huidigeStaat;
	}
	
	public ProductKorting getHuidigeKorting() {
		return huidigeKorting;
	}
	
	public String getHuidigeKortingType() {
		return getHuidigeKorting().getType();
	}
	
	public void setHuidigeKorting(ProductKorting huidigeKorting) {
		this.huidigeKorting = huidigeKorting;
	}
	
	public BigDecimal berekenKorting(int aantalDagen, BigDecimal totaalHuurprijs) {
		return this.getHuidigeKorting().berekenKorting(aantalDagen, totaalHuurprijs);
	}

	public boolean isBeschadigd() {
		return beschadigd;
	}

	public void setBeschadigd(boolean beschadigd) {
		this.beschadigd = beschadigd;
	}

	public abstract String getType();
	public abstract BigDecimal calculateRentalPrice(int aantalDagen);
	
	public ProductState getUitleenbaarState() {
		return uitleenbaarState;
	}

	public ProductState getUitgeleendState() {
		return uitgeleendState;
	}

	public ProductState getBeschadigdState() {
		return beschadigdState;
	}

	public ProductState getVerwijderdState() {
		return verwijderdState;
	}

	public String verwijderen() {
		return huidigeStaat.verwijderen();
	}
	
	public String uitlenen() {
		return huidigeStaat.uitlenen();
	}
	
	public String terugbrengen() {
		return huidigeStaat.terugbrengen();
	}
	
	public String herstellen() {
		return huidigeStaat.herstellen();
	}

}
