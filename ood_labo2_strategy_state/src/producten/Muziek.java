package producten;

import java.math.BigDecimal;

import domain.DomainException;
import enums.TProduct;

public class Muziek extends Product {

	public Muziek(String id, String name, BigDecimal waarde) throws DomainException
	{
		super(id, name, waarde);
	}

	@Override
	public BigDecimal calculateRentalPrice(int aantalDagen)
	{
		return new BigDecimal(aantalDagen * 1.5);
	}
	
	public String toString()
	{
		return getType() + " " + getName();
	}

	@Override
	public String getType()
	{
		return TProduct.MUZIEK.name();
	}

}
