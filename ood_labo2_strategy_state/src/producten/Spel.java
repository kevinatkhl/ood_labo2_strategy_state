package producten;

import java.math.BigDecimal;

import domain.DomainException;
import enums.TProduct;

public class Spel extends Product {

	public Spel(String id, String name, BigDecimal waarde) throws DomainException
	{
		super(id, name, waarde);
	}

	@Override
	public BigDecimal calculateRentalPrice(int aantalDagen)
	{
		return new BigDecimal(aantalDagen * 3);
	}
	
	public String toString()
	{
		return getType() + " " + getName();
	}

	@Override
	public String getType()
	{		
		return TProduct.SPEL.name();
	}

}
