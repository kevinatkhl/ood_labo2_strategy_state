package producten;

import java.math.BigDecimal;

import domain.DomainException;
import enums.TProduct;

public class Film extends Product {
	
	public Film(String id, String name, BigDecimal waarde) throws DomainException
	{
		super(id, name, waarde);
	}

	@Override
	public BigDecimal calculateRentalPrice(int aantalDagen)
	{
		int daysLeft = aantalDagen - 3;
		BigDecimal bd = daysLeft > 0 ? new BigDecimal(5 + (daysLeft * 2)) : new BigDecimal(5);
		return bd;
	}
	
	public String toString()
	{
		return getType() + " " + getName();
	}

	@Override
	public String getType()
	{		
		return TProduct.FILM.name();
	}

}
