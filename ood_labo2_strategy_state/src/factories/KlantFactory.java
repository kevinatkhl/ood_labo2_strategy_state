package factories;

import klanten.Klant;
import domain.DomainException;

public class KlantFactory {
	
	private volatile static KlantFactory uniqueInstance;
	
	private KlantFactory()
	{
		
	}
	
	public static KlantFactory getInstance()
	{
		if (uniqueInstance == null)
		{
			synchronized (KlantFactory.class)
			{
				if (uniqueInstance == null)
				{
					uniqueInstance = new KlantFactory();
				}
			}
		}
		return uniqueInstance;
	}
	
	public Klant createKlant(String naam, String email) throws DomainException
	{
		Klant klant = new Klant(naam, email);
		return klant;
	}

}
