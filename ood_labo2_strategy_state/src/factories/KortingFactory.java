package factories;

import korting.ProductKorting;
import domain.DomainException;

public class KortingFactory {
	
	private volatile static KortingFactory uniqueInstance;
	
	private KortingFactory()
	{
		
	}
	
	public static KortingFactory getInstance()
	{
		if (uniqueInstance == null)
		{
			synchronized (KortingFactory.class)
			{
				if (uniqueInstance == null)
				{
					uniqueInstance = new KortingFactory();
				}
			}
		}
		return uniqueInstance;
	}
	
	public ProductKorting create(String fullyQualifiedName) throws DomainException
	{
		Class<?> concreteClass;
		ProductKorting concreteDiscount = null;
		
		try {
			concreteClass = Class.forName(fullyQualifiedName);
			concreteDiscount = (ProductKorting) concreteClass.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return concreteDiscount;
	}
	
}
