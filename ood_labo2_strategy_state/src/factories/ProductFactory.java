package factories;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import producten.Product;
import states.ProductState;
import domain.DomainException;
import enums.TProductState;

public class ProductFactory {
	
	private volatile static ProductFactory uniqueInstance;
	
	private ProductFactory() {
		
	}
	
	public static ProductFactory getInstance()
	{
		if (uniqueInstance == null)
		{
			synchronized (ProductFactory.class)
			{
				if (uniqueInstance == null)
				{
					uniqueInstance = new ProductFactory();
				}
			}
		}
		return uniqueInstance;
	}
	
	public Product create(String fullyQualifiedName, String id, String naam, BigDecimal waarde) throws DomainException
	{
		Class<?> concreteClass;
		Product concreteProduct = null;
		Constructor<?> concreteConstructor;
		Class<?>[] parameters = new Class[]{String.class, String.class, BigDecimal.class};
		Object[] values = new Object[]{id, naam, waarde};
		
		try {
			concreteClass = Class.forName(fullyQualifiedName);
			concreteConstructor = concreteClass.getConstructor(parameters);
			concreteProduct = (Product) concreteConstructor.newInstance(values);
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return concreteProduct;
	}
	
	public Product create(String fullyQualifiedName, String id, String naam, BigDecimal waarde, String staat) throws DomainException
	{
		Product concreteProduct = create(fullyQualifiedName, id, naam, waarde);
		
		try {
			ProductStateFactory psFactory = ProductStateFactory.getInstance();
			TProductState productState = TProductState.valueOf(staat);
			String stateFullyQualifiedName = productState.getFullyQualifiedName();
			ProductState currState = psFactory.create(stateFullyQualifiedName, concreteProduct);
			concreteProduct.setHuidigeStaat(currState);
		} catch (DomainException e) {
			throw new DomainException("Error while trying to assign state to product.");
		}
		
		return concreteProduct;
	}
	
}