package factories;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import domain.DomainException;
import producten.Product;
import states.ProductState;

public class ProductStateFactory {
	
	private volatile static ProductStateFactory uniqueInstance;
	
	private ProductStateFactory()
	{
		
	}
	
	public static ProductStateFactory getInstance()
	{
		if (uniqueInstance == null)
		{
			synchronized (ProductStateFactory.class)
			{
				if (uniqueInstance == null)
				{
					uniqueInstance = new ProductStateFactory();
				}
			}
		}
		return uniqueInstance;
	}
	
	public ProductState create(String fullyQualifiedName, Product product) throws DomainException
	{
		Class<?> concreteClass;
		ProductState concreteState = null;
		Constructor<?> concreteConstructor;
		Class<?>[] parameters = new Class[]{Product.class};
		Object[] values = new Object[]{product};
		try {
			concreteClass = Class.forName(fullyQualifiedName);
			concreteConstructor = concreteClass.getConstructor(parameters);
			concreteState = (ProductState) concreteConstructor.newInstance(values);
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return concreteState;
	}
	
}
