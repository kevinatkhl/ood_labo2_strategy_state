package factories;

import db.DBReader;
import db.DBWriter;
import domain.DomainException;

public class DBFactory {

	public static volatile DBFactory uniqueInstance;
	
	public DBFactory()
	{
		
	}
	
	public static DBFactory getInstance()
	{
		if (uniqueInstance == null)
		{
			synchronized (DBFactory.class)
			{
				if (uniqueInstance == null)
				{
					uniqueInstance = new DBFactory();
				}
			}
		}
		return uniqueInstance;
	}
	
	public DBWriter createWriter(String fullyQualifiedName) throws DomainException
	{
		Class<?> concreteClass;
		DBWriter concreteWriter = null;
		
		try {
			concreteClass = Class.forName(fullyQualifiedName);
			concreteWriter = (DBWriter) concreteClass.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return concreteWriter;
	}
	
	public DBReader createReader(String fullyQualifiedName) throws DomainException
	{
		Class<?> concreteClass;
		DBReader concreteReader = null;
		
		try {
			concreteClass = Class.forName(fullyQualifiedName);
			concreteReader = (DBReader) concreteClass.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return concreteReader;
	}
	
}
