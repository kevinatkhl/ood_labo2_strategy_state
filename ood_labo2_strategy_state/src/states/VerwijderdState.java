package states;

import enums.TProductState;
import producten.Product;

public class VerwijderdState implements ProductState {

	Product product;
	
	public VerwijderdState(Product product) {
		setProduct(product);
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String verwijderen() {
		return "Dit product is al verwijderd.";
	}

	@Override
	public String uitlenen() {
		return "Je mag een verwijderd product niet uitlenen.";
	}

	@Override
	public String terugbrengen() {
		return "Een verwijderd product kan niet teruggebracht worden.";
	}

	@Override
	public String herstellen() {
		return "Een verwijderd product kan niet hersteld worden.";
	}

	@Override
	public String getStateType() {
		return TProductState.VERWIJDERD.name();
	}

}
