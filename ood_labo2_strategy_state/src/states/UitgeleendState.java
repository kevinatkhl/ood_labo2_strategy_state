package states;

import enums.TProductState;
import producten.Product;

public class UitgeleendState implements ProductState {

	Product product;
	
	public UitgeleendState(Product product) {
		setProduct(product);
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String verwijderen() {
		return "Product: " + product + " is momenteel uitgeleend en kan niet verwijderd worden.";
	}

	@Override
	public String uitlenen() {
		return "Product: " + product + " is al uitgeleend.";
	}

	@Override
	public String terugbrengen() {
		if (getProduct().isBeschadigd()) {
			getProduct().setHuidigeStaat(getProduct().getBeschadigdState());
			return "Product: " + product + " werd beschadigd teruggebracht.";
		} else {
			getProduct().setHuidigeStaat(getProduct().getUitleenbaarState());
			return "Product: " + product + " werd teruggebracht en kan opnieuw worden uitgeleend.";
		}
	}

	@Override
	public String herstellen() {
		// TODO Auto-generated method stub
		return "Product : " + product + " is momenteel uitgeleend en kan niet hersteld worden.";
	}
	
	@Override
	public String getStateType()
	{
		return TProductState.UITGELEEND.name();
	}

}
