package states;

import enums.TProductState;
import producten.Product;

public class BeschadigdState implements ProductState {
	
	Product product;
	
	public BeschadigdState(Product product) {
		setProduct(product);
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String verwijderen() {
		product.setHuidigeStaat(product.getVerwijderdState());
		return "Product : " + product + " verwijderd.";
	}

	@Override
	public String uitlenen() {
		return "Product: " + product + " is beschadigd en kan niet uitgeleend worden.";
	}

	@Override
	public String terugbrengen() {
		return "Product: " + product + " is al in de winkel.";
	}

	@Override
	public String herstellen() {
		getProduct().setBeschadigd(false);
		getProduct().setHuidigeStaat(getProduct().getUitleenbaarState());
		return "Product: " + product + " werd hersteld en kan terug uitgeleend worden.";
	}
	
	@Override
	public String getStateType() {
		return TProductState.BESCHADIGD.name();
	}

}
