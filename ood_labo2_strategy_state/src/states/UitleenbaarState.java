package states;

import enums.TProductState;
import producten.Product;

public class UitleenbaarState implements ProductState {
	
	Product product;

	public UitleenbaarState(Product product) {
		setProduct(product);
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String verwijderen() {
		getProduct().setHuidigeStaat(product.getVerwijderdState());
		return "Product: " + product + " verwijderd.";
	}

	@Override
	public String uitlenen() {
		getProduct().setHuidigeStaat(product.getUitgeleendState());
		return "Product: " + product + " wordt nu uitgeleend.";
	}

	@Override
	public String terugbrengen() {
		return "Product: " + product + " is nog niet uitgeleend.";
	}

	@Override
	public String herstellen() {
		return "Product: " + product + " is niet beschadigd.";
	}

	@Override
	public String getStateType() {
		return TProductState.UITLEENBAAR.name();
	}

}
