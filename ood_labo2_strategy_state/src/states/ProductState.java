package states;

public interface ProductState {
	
	String verwijderen();
	String uitlenen();
	String terugbrengen();
	String herstellen();
	String getStateType();

}
