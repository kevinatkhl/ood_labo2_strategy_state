package db;

public interface DBWriter {
	
	void write() throws DbException;

}
