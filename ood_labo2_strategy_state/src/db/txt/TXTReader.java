package db.txt;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.Scanner;

import producten.Product;
import db.DBHandler;
import db.DBReader;
import db.DbException;
import domain.DomainException;
import enums.TProduct;
import factories.ProductFactory;

public class TXTReader implements DBReader {
	
	@Override
	public void read() throws DbException {
		Scanner fileScanner = null;
		
		try {
			fileScanner = new Scanner(DBHandler.getBestand());
		} catch (FileNotFoundException e) {
			throw new DbException("File not available.");
		}
		
		while (fileScanner.hasNextLine())
		{
			Scanner lineScanner = null;
			try {
				lineScanner = new Scanner(fileScanner.nextLine());
				lineScanner.useDelimiter("\t");
				String type			= lineScanner.next();
				String id			= lineScanner.next();
				BigDecimal waarde	= new BigDecimal(Double.parseDouble(lineScanner.next()));
				String naam			= lineScanner.next();
				String staat		= lineScanner.next();
				try {
					ProductFactory productFactory = ProductFactory.getInstance();
					TProduct productType = TProduct.valueOf(type);
					String fullyQualifiedName = productType.getFullyQualifiedName();
					Product product = productFactory.create(fullyQualifiedName, id, naam, waarde, staat);
					DBHandler.getWinkel().addProduct(product);
				} catch (DomainException e) {
					e.printStackTrace();
				} finally {
					lineScanner.close();
				}
			} finally {
				fileScanner.close();
			}
		}		
	}

}
