package db.txt;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import db.DBHandler;
import db.DBWriter;
import db.DbException;
import enums.TProductState;
import producten.Product;

public class TXTWriter implements DBWriter {

	@Override
	public void write() throws DbException
	{
		PrintWriter writer;
		
		try {
			writer = new PrintWriter(DBHandler.getBestand());
		} catch (FileNotFoundException e) {
			throw new DbException(e);
		}
		
		List<Product> producten = DBHandler.getWinkel().getProducten();
		
		for (Product product : producten)
		{
			if (!product.getHuidigeStaat().equals(TProductState.VERWIJDERD.getProductState()))
			{
				writer.print(product.getType());
				writer.print("\t");
				writer.print(product.getId());
				writer.print("\t");
				writer.print(product.getWaarde());
				writer.print("\t");
				writer.print(product.getName());
				writer.print("\t");
				writer.println(product.getHuidigeStaatType());
			}
		}

		writer.close();
	}

}
