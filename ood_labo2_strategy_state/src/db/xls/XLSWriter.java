package db.xls;

import java.io.IOException;
import java.util.ArrayList;

import db.DBHandler;
import db.DBWriter;
import db.DbException;
import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import producten.Product;

public class XLSWriter implements DBWriter {

	private ExcelHandler xlsHandler = new ExcelHandler();
	
	public XLSWriter()
	{
	}

	@Override
	public void write() throws DbException {
		ArrayList<ArrayList<String>> info = new ArrayList<ArrayList<String>>();
		for (Product p : DBHandler.getWinkel().getProducten())
		{
			ArrayList<String> rowinfo = new ArrayList<String>();
			rowinfo.add(p.getType());
			rowinfo.add(p.getId());
			rowinfo.add(p.getName());
			rowinfo.add(p.getWaarde().toString());
			rowinfo.add(p.getHuidigeStaatType());
			info.add(rowinfo);
		}
		try {
			xlsHandler.write(DBHandler.getBestand(), info);
		} catch (BiffException | WriteException | IOException e) {
			e.printStackTrace();
		}
	}
	
}
