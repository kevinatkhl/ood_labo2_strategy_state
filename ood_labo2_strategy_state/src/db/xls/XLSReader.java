package db.xls;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import db.DBHandler;
import db.DBReader;
import db.DbException;
import domain.DomainException;
import producten.Product;
import enums.TProduct;
import factories.ProductFactory;
import jxl.read.biff.BiffException;

public class XLSReader implements DBReader {
	
	private ExcelHandler xlsHandler = new ExcelHandler();
	
	public XLSReader()
	{
	}

	@Override
	public void read() throws DbException {
		try {
			ArrayList<ArrayList<String>> info = xlsHandler.read(DBHandler.getBestand());
			for (List<String> infoCell : info)
			{
				String type = null;
				String id = null;
				BigDecimal waarde = null;
				String title = null;
				String staat = null;
				String[] productLine = new String[5];
				for (int i=0; i<infoCell.size(); i++)
				{
					productLine[i] = infoCell.get(i);
				}
				type = productLine[0];
				id = productLine[1];
				title = productLine[2];
				waarde = new BigDecimal(Double.parseDouble(productLine[3]));
				staat = productLine[4];
				
				ProductFactory productFactory = ProductFactory.getInstance();
				TProduct productType = TProduct.valueOf(type);
				String fullyQualifiedName = productType.getFullyQualifiedName();
				Product product;
				try {
					product = productFactory.create(fullyQualifiedName, id, title, waarde, staat);
					DBHandler.getWinkel().addProduct(product);
				} catch (DomainException e) {
					e.printStackTrace();
				}
			}
		} catch (BiffException | IOException e) {
			e.printStackTrace();
		}
	}

}
