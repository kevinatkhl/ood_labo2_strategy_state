package db;

public interface DBReader {

	void read() throws DbException;
	
}
