package db.xml;

public class XmlTags {
	
	static final String WINKEL = "winkel";
	
	static final String PRODUCTEN = "producten";
	static final String PRODUCT = "product";
	static final String TYPE = "type";
	static final String ID = "id";
	static final String TITLE = "title";
	static final String WAARDE = "waarde";
	static final String STATE = "state";
	
	static final String KLANTEN = "klanten";
	static final String USERID = "userid";
	static final String KLANT = "klant";
	static final String NAAM = "naam";
	static final String EMAIL = "email";

}
