package db.xml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import klanten.Klant;
import producten.Product;
import db.DBHandler;
import db.DBReader;
import db.DbException;
import domain.DomainException;
import enums.TProduct;
import factories.KlantFactory;
import factories.ProductFactory;


public class XMLReader implements DBReader {

	@Override
	public void read() throws DbException {
		try {
			// First create a new XMLInputFactory
		    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		    // Setup a new eventReader
		    InputStream in = new FileInputStream(DBHandler.getBestand());
		    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
		    // Read the XML document
		    String type			= null;
	        String id			= null;
	        BigDecimal waarde	= null;
	        String title		= null;
	        String staat		= null;
	        
	        String userID		= null;
	        String naam			= null;
	        String email		= null;
	        
	        while (eventReader.hasNext()) {
	        	XMLEvent event = eventReader.nextEvent();
	        	if (event.isStartElement()) {
	        		StartElement sE = event.asStartElement();
	        		
	        		if (sE.getName().getLocalPart() == (XmlTags.PRODUCT)) {
	        			
	        		}
	        		else if (sE.getName().getLocalPart().equals(XmlTags.TYPE)) {
	        			event = eventReader.nextEvent();
	        			type = event.asCharacters().getData();
	        		}
	        		else if (sE.getName().getLocalPart().equals(XmlTags.ID)) {
	        			event = eventReader.nextEvent();
	        			id = event.asCharacters().getData();
	        		}
	        		else if (sE.getName().getLocalPart().equals(XmlTags.TITLE)) {
	        			event = eventReader.nextEvent();
	        			title = event.asCharacters().getData();
	        		}
	        		else if (sE.getName().getLocalPart().equals(XmlTags.WAARDE)) {
	        			event = eventReader.nextEvent();
	        			waarde = new BigDecimal(Double.parseDouble(event.asCharacters().getData()));
	        		}
	        		else if (sE.getName().getLocalPart().equals(XmlTags.STATE)) {
	        			event = eventReader.nextEvent();
	        			staat = event.asCharacters().getData();
	        		}
	        		
	        		if (sE.getName().getLocalPart() == (XmlTags.KLANT)) {
	        			
	        		}
	        		else if (sE.getName().getLocalPart().equals(XmlTags.USERID)) {
	        			event = eventReader.nextEvent();
	        			userID = event.asCharacters().getData();
	        		}
	        		else if (sE.getName().getLocalPart().equals(XmlTags.NAAM)) {
	        			event = eventReader.nextEvent();
	        			naam = event.asCharacters().getData();
	        		}
	        		else if (sE.getName().getLocalPart().equals(XmlTags.EMAIL)) {
	        			event = eventReader.nextEvent();
	        			email = event.asCharacters().getData();
	        		}
	        	}
	        	else if (event.isEndElement()) {
	        		EndElement endElement = event.asEndElement();
	        		if (endElement.getName().getLocalPart() == (XmlTags.PRODUCT)) {
	        			try {
	        				ProductFactory productFactory = ProductFactory.getInstance();
	        				TProduct productType = TProduct.valueOf(type);
	        				String fullyQualifiedName = productType.getFullyQualifiedName();
	        				Product product = productFactory.create(fullyQualifiedName, id, title, waarde, staat);
	        				DBHandler.getWinkel().addProduct(product);
	        			} catch (DomainException e) {
	        				throw new DbException("Failed to add product to file.");
	        			}
	        		}
	        		else if (endElement.getName().getLocalPart() == (XmlTags.KLANT)) {
	        			KlantFactory klantFactory = KlantFactory.getInstance();
						Klant klant;
						try {
							klant = klantFactory.createKlant(naam, email);
						} catch (DomainException e) {
							throw new DbException("Failed to add klant to file.");
						}
						DBHandler.getWinkel().addKlant(klant);
	        		}
	        	}
	        }
		} catch (FileNotFoundException e) {
			throw new DbException("File not found.");
		} catch (XMLStreamException e){
			throw new DbException("File has wrong format.");
		}
	}
	
}