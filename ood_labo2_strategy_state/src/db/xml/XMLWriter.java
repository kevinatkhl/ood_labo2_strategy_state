package db.xml;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import db.DBHandler;
import db.DBWriter;
import db.DbException;
import klanten.Klant;
import producten.Product;

public class XMLWriter implements DBWriter {

	@Override
	public void write() throws DbException {
		
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLEventWriter eventWriter;
		
		try {
			eventWriter = outputFactory.createXMLEventWriter(new FileOutputStream(DBHandler.getBestand()));
		} catch (FileNotFoundException | XMLStreamException e) {
			throw new DbException("Invalid file.");
		}
		
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
		XMLEvent end = eventFactory.createDTD("\n");
		
		StartDocument startDocument = eventFactory.createStartDocument();
		
		try
		{
			eventWriter.add(startDocument);
			eventWriter.add(end);
			
			StartElement winkelStartElement = eventFactory.createStartElement("", "", XmlTags.WINKEL);
			eventWriter.add(winkelStartElement);
			
			StartElement productenStartElement = eventFactory.createStartElement("", "", XmlTags.PRODUCTEN);
			eventWriter.add(productenStartElement);
			eventWriter.add(end);
			
			ArrayList<Product> producten = DBHandler.getWinkel().getProducten();
			
			for (Product product : producten)
			{
				eventWriter.add(eventFactory.createStartElement("", "", XmlTags.PRODUCT));
				eventWriter.add(end);
				
				createNode(eventWriter, XmlTags.TYPE, product.getType());
				createNode(eventWriter, XmlTags.ID, product.getId());
				createNode(eventWriter, XmlTags.TITLE, product.getName());
				createNode(eventWriter, XmlTags.WAARDE, product.getWaarde().toString());
				createNode(eventWriter, XmlTags.STATE, product.getHuidigeStaatType());
				
				eventWriter.add(eventFactory.createEndElement("", "", XmlTags.PRODUCTEN));
				eventWriter.add(end);
			}
			
			eventWriter.add(eventFactory.createEndElement("", "", XmlTags.PRODUCT));
			
			StartElement klantenStartElement = eventFactory.createStartElement("", "", XmlTags.KLANTEN);
			eventWriter.add(klantenStartElement);
			eventWriter.add(end);
			
			ArrayList<Klant> klanten = DBHandler.getWinkel().getKlantenList();
			
			for (Klant klant : klanten)
			{
				eventWriter.add(eventFactory.createStartElement("", "", XmlTags.KLANT));
				eventWriter.add(end);
				
				createNode(eventWriter, XmlTags.ID, Integer.toString(klant.getUserId()));
				createNode(eventWriter, XmlTags.NAAM, klant.getNaam());
				createNode(eventWriter, XmlTags.EMAIL, klant.getEmail());
				
				eventWriter.add(eventFactory.createEndElement("", "", XmlTags.KLANT));
				eventWriter.add(end);
			}
			
			eventWriter.add(eventFactory.createEndElement("", "", XmlTags.KLANTEN));
			
			eventWriter.add(eventFactory.createEndElement("", "", XmlTags.WINKEL));
			eventWriter.add(eventFactory.createEndDocument());
			eventWriter.close();
		} catch (XMLStreamException e) {
			throw new DbException("Wrong format.");
		}

	}
		
	private void createNode(XMLEventWriter eventWriter, String name, String value) 
		throws XMLStreamException {
	    XMLEventFactory eventFactory = XMLEventFactory.newInstance();
	    XMLEvent end = eventFactory.createDTD("\n");
	    XMLEvent tab = eventFactory.createDTD("\t");
	    // Create Start node
	    StartElement sElement = eventFactory.createStartElement("", "", name);
	    eventWriter.add(tab);
	    eventWriter.add(sElement);
	    // Create Content
	    Characters characters = eventFactory.createCharacters(value);
	    eventWriter.add(characters);
	    // Create End node
	    EndElement eElement = eventFactory.createEndElement("", "", name);
	    eventWriter.add(eElement);
	    eventWriter.add(end);
	}

}
