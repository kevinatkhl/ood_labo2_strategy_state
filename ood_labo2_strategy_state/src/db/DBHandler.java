package db;

import java.io.File;

import domain.Winkel;

public class DBHandler {
	
	private static File bestand;
	private static Winkel winkel;
	private DBReader reader;
	private DBWriter writer;
	
	public DBHandler(String bestand, Winkel winkel) throws DbException {
		setBestand(bestand);
		setWinkel(winkel);
	}
	
	public static File getBestand() { return bestand; }
	
	private void setBestand(String bestand) throws DbException {
		try {
			DBHandler.bestand = new File(bestand);
		} catch (NullPointerException e) {
			throw new DbException("Invalid file.");
		}
	}
	
	public static Winkel getWinkel() { return winkel; }
	
	private void setWinkel(Winkel winkel) { DBHandler.winkel = winkel; }
	
	public DBReader getReader() { return reader; }
	public void setReader(DBReader reader) { this.reader = reader; }
	
	public DBWriter getWriter() { return writer; }
	public void setWriter(DBWriter writer) { this.writer = writer; }
	
}
