package klanten;

import java.util.LinkedList;
import java.util.Queue;

import domain.Observer;
import domain.Winkel;
import producten.Product;

public class Klant implements Observer {
	
	private String	naam,
					email;
	private static int nextMember = 0;
	private int userId;
	
	private Winkel winkel;
	
	private Queue<Product> queue;
	
	public Klant(String naam, String email) {
		setNaam(naam);
		setEmail(email);
		setWinkel(winkel);
		nextMember += 1;
		userId = nextMember;
		queue = new LinkedList<Product>();
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getUserId() {
		return userId;
	}
	
	public Winkel getWinkel() {
		return winkel;
	}
	
	public void setWinkel(Winkel winkel) {
		this.winkel = winkel;
	}
	
	public Queue<Product> getQueue() {
		return queue;
	}
	
	@Override
	public void update(Product recentProduct) {
		if (getQueue().size() >= 3) {
			getQueue().poll();
			getQueue().offer(recentProduct);
		} else {
			getQueue().offer(recentProduct);
		}
	}
	
	public String printFQ() {
		StringBuilder sb = new StringBuilder();
		for (Product p : getQueue()) {
			sb.append(p.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
	
}
